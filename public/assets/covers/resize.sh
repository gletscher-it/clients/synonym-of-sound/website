#!/bin/sh
# Resizes cover images (*.[jpg,png]) in current directory to 392x392

if ! command -v convert > /dev/null 2>&1; then
  printf "Please install ImageMagick (https://imagemagick.org/)\n"
  exit
fi

for f in *.jpg *.png; do
  case $f in
    *.392.jpg);;
    *.392.png);;
    *)
      convert "$f" -resize 392x392 "${f%.*}.392.${f#*.}"
      printf " ☑ %s\n" "${f%.*}.392.${f#*.}"
      ;;
  esac
done

