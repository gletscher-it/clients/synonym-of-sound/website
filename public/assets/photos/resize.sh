#!/bin/sh
# Resizes images (*.[jpg,png]) in current directory to ?x720

if ! command -v convert > /dev/null 2>&1; then
  printf "Please install ImageMagick (https://imagemagick.org/)\n"
  exit
fi

for f in *.jpg *.png; do
  case $f in
    *.720.*);;
    *.1080.*);;
    *)
      convert "$f" -resize 'x720' "${f%.*}.720.${f#*.}"
      convert "$f" -resize 'x1080' "${f%.*}.1080.${f#*.}"
      printf " ☑ %s\n" "${f%.*}.*.${f#*.}"
      ;;
  esac
done

